package com.poapper.demo.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class htmlTest {
    @GetMapping("/html")
    public String test(){
        return "index";
    }
}
