package com.poapper.demo.controller;

import com.poapper.demo.domain.Member;
import com.poapper.demo.dto.MailDto;
import com.poapper.demo.service.MailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class MailController {
    private final MailService mailService;

    @GetMapping("/mail")
    public String dispMail() {
        return "mail";
    }

    @PostMapping("/mail")
    public void execMail(MailDto mailDto) {
        mailService.mailSend(mailDto);
    }

    @ResponseBody
    @RequestMapping("/send")
    public String sendMail(){
        MailDto mailDto=new MailDto();
        mailDto.setAddress("roxy1029@naver.com");
        mailDto.setMessage("hello");
        mailDto.setTitle("가입 인증 메일");
        mailService.mailSend(mailDto);
        return "send";
    }


}
