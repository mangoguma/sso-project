package com.ssoproject.assn2.domain;

import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_member")
public class Member {
    @Id
    private String Id;

    private String Name;

    private String Password;

    public String getId(){
        return this.Id;
    }

    //@Column
    //private String Password;

}
