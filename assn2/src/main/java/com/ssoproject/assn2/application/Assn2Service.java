package com.ssoproject.assn2.application;

import com.ssoproject.assn2.domain.Member;
import com.ssoproject.assn2.dto.MemberDto;
import com.ssoproject.assn2.infra.MemberRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class Assn2Service {
    private final MemberRepository memberRepository;

    public Assn2Service(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }


    public List<Member> findAll(){
        return memberRepository.findAll();
    }

    public String add(MemberDto memberDto){
        Optional<Member> findMember = memberRepository.findById(memberDto.getId());
        if (findMember.isPresent()){
            return findMember.get().getName()+" is already exsisted";
        }else {
            Member member = Member.builder()
                    .Id(memberDto.getId())
                    .Name(memberDto.getName())
                    .Password(memberDto.getPassword())
                    .build();
            memberRepository.save(member);
            return "added";
        }
    }

    public String find(String id) {
        Optional<Member> findMember = memberRepository.findById(id);
        if (findMember.isPresent()) {
            Member member = findMember.get();
            return "id: " + member.getId() + ", name: " + member.getName();
        }else{
            return "no existed";
        }
    }

    public String update(MemberDto memberDto){
        Optional<Member> findMember = memberRepository.findById(memberDto.getId());
        if (findMember.isPresent()){
            Member member = Member.builder()
                    .Id(memberDto.getId())
                    .Name(memberDto.getName())
                    .Password(memberDto.getPassword())
                    .build();
            memberRepository.save(member);
            return "updated";
        }else {
            return "no exsisted";
        }
    }

    public String delete(String id){
        Optional<Member> findMember = memberRepository.findById(id);
        if (findMember.isPresent()){
            memberRepository.deleteById(id);
            return "deleted";
        }else {
            return "no exsisted";
        }
    }
}
