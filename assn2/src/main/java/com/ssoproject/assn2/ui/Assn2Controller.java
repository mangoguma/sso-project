package com.ssoproject.assn2.ui;

import com.ssoproject.assn2.application.Assn2Service;
import com.ssoproject.assn2.domain.Member;
import com.ssoproject.assn2.dto.MemberDto;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class Assn2Controller {
    @Autowired
    private Assn2Service assn2Service;

    @RequestMapping(value = "/")
    public ModelAndView index(){
        ModelAndView modelAndView = getDefaultModelAndView("");
        return modelAndView;
    }


    @GetMapping("/add")
    public ModelAndView getAddForm(){
        ModelAndView modelAndView = getDefaultModelAndView("add");
        return modelAndView;
    }

    @PostMapping("/add")
    public ModelAndView getAddResultForm(
            MemberDto memberDto
    ){
        ModelAndView modelAndView = getDefaultModelAndView("add");
        modelAndView.addObject("msg", this.assn2Service.add(memberDto));
        return modelAndView;
    }


    @GetMapping("/findId")
    public ModelAndView getFindForm(){
        ModelAndView modelAndView = getDefaultModelAndView("findId");
        return modelAndView;
    }

    @PostMapping("/findId")
    public ModelAndView getFindResultForm(
            MemberDto memberDto
    ){
        ModelAndView modelAndView = getDefaultModelAndView("findId");
        modelAndView.addObject("msg", this.assn2Service.find(memberDto.getId()));
        return modelAndView;
    }


    @GetMapping("/delete")
    public ModelAndView getDeleteForm(){
        ModelAndView modelAndView = getDefaultModelAndView("delete");
        return modelAndView;
    }

    @PostMapping("/delete")
    public ModelAndView getDeleteResultForm(
            MemberDto memberDto
    ){
        ModelAndView modelAndView = getDefaultModelAndView("delete");
        modelAndView.addObject("msg", this.assn2Service.delete(memberDto.getId()));
        return modelAndView;
    }


    @GetMapping("/update")
    public ModelAndView getUpdateForm(){
        ModelAndView modelAndView = getDefaultModelAndView("update");
        return modelAndView;
    }

    @PostMapping("/update")
    public ModelAndView getUpdateResultForm(
            MemberDto memberDto
    ){
        ModelAndView modelAndView = getDefaultModelAndView("update");
        modelAndView.addObject("msg", this.assn2Service.update(memberDto));
        return modelAndView;
    }


    @GetMapping("/all")
    public ModelAndView getAllMembers(){
        ModelAndView modelAndView = new ModelAndView("list");
        List<MemberDto> members = this.assn2Service.findAll().stream()
                .map(member -> MemberDto.builder()
                        .Id(member.getId())
                        .Name(member.getName())
                        .Password(member.getPassword())
                        .build()
                )
                .collect(Collectors.toList());
        modelAndView.addObject("members",members);
        return  modelAndView;
    }


    private ModelAndView getDefaultModelAndView(String mode) {
        ModelAndView modelAndView = new ModelAndView("/index");
        modelAndView.addObject("mode", mode);
        modelAndView.addObject("msg", "");
        modelAndView.addObject("id", "");
        modelAndView.addObject("name", "");
        modelAndView.addObject("password","");
        return modelAndView;
    }

}