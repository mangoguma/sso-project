package com.ssoproject.assn2.ui;

import com.ssoproject.assn2.domain.Member;
import com.ssoproject.assn2.infra.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class Assn3Controller {

    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;


    @GetMapping("/main")
    public String mainPage(Map<String, Object> model) {
        List<Member> members = memberRepository.findAll();
        model.put("members", members);
        return "homepage";
    }

    @GetMapping("/admin")
    public String adminPage(Map<String, Object> model) {
        return "adminpage";
    }

    @GetMapping("/member/new")
    public String memberJoinForm(Member memberForm) {
        return "memberJoinForm";
    }

    @PostMapping("/member/new")
    public String memberJoin(Member memberForm) {
        memberRepository.save(memberForm);
        return "redirect:/login";
    }

//    @GetMapping("/main")
//    public String mainPage(@AuthenticationPrincipal Member member,
//                           Map<String, Object> model) {
//        List<Member> members = memberRepository.findAll();
//        model.put("members", members);
//        model.put("currentMemberId", member.getName());
//        return "homepage";
//    }
}
