package com.ssoproject.assn2.ui;

        import com.ssoproject.assn2.application.Assn2Service;
        import com.ssoproject.assn2.domain.Member;
        import com.ssoproject.assn2.dto.MemberDto;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;

        import java.util.List;
        import java.util.Optional;

@RestController
@RequestMapping("api")
public class Assn2RestController {
    @Autowired
    private Assn2Service assn2Service;

    @GetMapping(value = "/all")
    public List<Member> AllMember() {
        return assn2Service.findAll();
    }

    @GetMapping(value = "/add")
    public String AddMember(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "password") String password) {
        MemberDto memberDto = new MemberDto(id, name, password);
        return assn2Service.add(memberDto);
    }

    @GetMapping(value = "/find")
    public String FindMember(@RequestParam(value = "id") String id) {
        return assn2Service.find(id);
    }

    @GetMapping(value = "/update")
    public String UpdateMember(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "password") String password) {
        MemberDto memberDto = new MemberDto(id, name, password);
        return assn2Service.update(memberDto);
    }

    @GetMapping(value = "/delete")
    public String DeleteMember(@RequestParam(value = "id") String id) {
        return assn2Service.delete(id);
    }



}
