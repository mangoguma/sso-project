package com.ssoproject.assn2.infra;

import com.ssoproject.assn2.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepository extends JpaRepository<Member, String> {
    public Optional<Member> findById(String id);

    public List<Member> findAll();

    @Transactional
    public void deleteById(String id);

}
