package com.ssoproject.assn2.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MemberDto {
    private String Id;
    private String Name;
    private String Password;
}
