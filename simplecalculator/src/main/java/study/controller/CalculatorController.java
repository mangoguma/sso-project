package study.controller;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;


@RestController
public class CalculatorController {

    @RequestMapping("/{operator}")
    public double calculator(@PathVariable String operator, @RequestParam(value = "num1") double num1, @RequestParam(value = "num2") double num2) {
        double ans = 0;
        switch (operator) {
            case "plus":
                ans = num1 + num2;
                break;
            case "subtract":
                ans = num1 - num2;
                break;
            case "multiply":
                ans = num1 * num2;
                break;
            case "division":
                ans = num1 / num2;
                break;
            default:
                ans = 0;

        }
        return ans;
    }
}

